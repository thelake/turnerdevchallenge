
/**
 * Turner Developer Challenge
 * version 1.0
 * James Lakey
 * Module dependencies.
 */

var express     = require('express')
    , models    = {}
    , http      = require('http')
    , fs        = require('fs')
    , path      = require('path');

var env = process.env.NODE_ENV || 'dev'
    , config = require('./config/config')[env]
    , mongoose = require('mongoose')

// Bootstrap models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function (file) {
    require(models_path+'/'+file)
})

// Bootstrap db connection
mongoose.connect(config.db);

// For cross-domain access
var allowCrossDomain = function(req,res,next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
    next();
}

// the app
var app = module.exports = express();

// the app configuration
app.configure(function(){
    app.set('port', process.env.PORT || 8080);
    app.set('views', __dirname + '/public/views');
    app.set('view engine', 'ejs');
    app.locals.pretty = true;
    app.use(express.favicon());
    app.use(express.logger('devchallenge'));
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(allowCrossDomain);
    app.use(express.static(path.join(__dirname, 'public')));
});

// the api routes
var titles = require(__dirname + '/app/controllers/titles');
app.get('/', titles.index);
app.get('/titles/all', titles.list);
app.get('/titles/detail/:name', titles.detail);
app.get('/titles/search/:searchterm', titles.search);

// start the server
http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
