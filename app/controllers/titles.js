var mongoose = require('mongoose')
    , Title = mongoose.model('Title');

exports.index = function(req,res){
//    res.send('app is here');
    res.render('index2')
}

exports.list = function(req, res) {
    Title.find({}, function(err, titles) {
        res.json(titles);
    });
}

exports.detail = function(req, res){
    console.log('rbn:' + req.params.name);
    Title.findOne({ TitleName: req.params.name }, function (err, detail) {
        console.log(detail);
        res.json(detail);
    });
}

exports.search = function(req, res){
    Title.find({TitleName: {$regex: req.params.searchterm }}, function (err, SearchResults) {
        console.log('rbd:' + req.params.searchterm);
        res.json(SearchResults);
    });
}