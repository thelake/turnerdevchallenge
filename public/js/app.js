var devChallengeApp = angular.module('devChallengeApp', []);

devChallengeApp.config(['$routeProvider',
    function($routeProvider, $scope, $http) {
        $routeProvider
            .when('/Detail',{
                templateUrl: 'detail',
                controller:'devchallenge'
            })
            .otherwise({
                redirectTo: '/'
            })
    },

    function ($scope, $http){

        $http.get('/titles/all').success(function(data){
            $scope.titles = data;
        });

        $scope.detail =  function(title){
            console.log("c:" + title.TitleName);
            $http.get('/titles/detail/' + title.TitleName, title ).success(function(data){
                console.log(data);
                $scope.title = data;
            });
        }

        $scope.searchFilter = function (obj) {
            var re = new RegExp($scope.searchText, 'i');
            return !$scope.searchText || re.test(obj.TitleName);
        };
    }
]);
