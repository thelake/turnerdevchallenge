angular.module('DevCh', ['ngRoute'],


function($routeProvider){
    $routeProvider
        .when('/Detail/:TitleName', {
            templateUrl: '../views/detail.ejs',
            controller: Dtail
        })
        .when('/Church/:id', {
            templateUrl: '../views/churchdetail.ejs',
            controller: Dtail
        })
        .when('/Churches', {
            templateUrl: '../views/churchdetail.ejs',
            controller: devchallenge
        })
        .otherwise({
            redirectTo: '/'
        });
});

function devchallenge ($scope, $http, $location){
//    $locationProvider.html5Mode(true).hashPrefix('!');

    $http.get('http://api.jlproductions.net/churches').success(function(data){
        $scope.titles = data;
    });

    $scope.detail =  function(title){
        console.log("title:" + title.TitleName);
        $http.get('/titles/detail/' + title.TitleName, title ).success(function(data){
            $scope.title = data;
        });
    }

    $scope.deets =  function(path, id){
 //       $http.get('http://api.jlproductions.net/churches/' + id ).success(function(data){
        console.log('detail-path: ' + path);
        console.log('detail-id: ' + id);
 //           console.log(data);
 //           $scope.title = data;
            $location.path('/Church/' + id);

 //       });
    }

    $scope.searchFilter = function (obj) {
        var re = new RegExp($scope.searchText, 'i');
        return !$scope.searchText || re.test(obj.church);
    };
}

function Dtail($scope, $http, $routeParams) {
    console.log($routeParams);
    $http.get('http://api.jlproductions.net/churches/' + $routeParams.id ).success(function(data){
        console.log('detail object');
        console.log(data);
        $scope.title = data;
    });

}



